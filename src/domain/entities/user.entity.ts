import { PrimaryGeneratedColumn, Entity, Column, Index } from 'typeorm';
import { BaseEntity } from './base.entity';

@Entity()
export class User extends BaseEntity {
  constructor(partial: Partial<User>) {
    super();
    Object.assign(this, partial);
  }

  @PrimaryGeneratedColumn('uuid')
  userId!: string;

  @Column({ type: 'varchar', nullable: false, length: 100 })
  @Index()
  fullName!: string;

  @Column({ type: 'varchar', nullable: false, length: 100 })
  @Index()
  username!: string;

  @Column({ type: 'varchar', nullable: false, length: 500 })
  password!: string;
}
