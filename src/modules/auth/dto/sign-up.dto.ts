import { IsNotEmpty, IsString, MaxLength } from 'class-validator';
import { Exclude } from 'class-transformer';
import { SignInDto, SignInResDto } from './sign-in.dto';

export class SignUpDto extends SignInDto {
  @IsString()
  @IsNotEmpty()
  @MaxLength(100)
  fullName!: string;
}

@Exclude()
export class SignUpResDto extends SignInResDto {}
