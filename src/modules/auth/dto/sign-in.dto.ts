import {
  IsNotEmpty,
  IsString,
  MaxLength,
  IsStrongPassword,
} from 'class-validator';
import { Exclude, Expose } from 'class-transformer';

export class SignInDto {
  @IsString()
  @IsNotEmpty()
  @MaxLength(100)
  username!: string;

  @IsString()
  @IsNotEmpty()
  @IsStrongPassword({
    minLength: 6,
    minLowercase: 1,
    minNumbers: 1,
    minSymbols: 1,
    minUppercase: 1,
  })
  password!: string;
}

@Exclude()
export class SignInResDto {
  @Expose()
  userId!: string;

  @Expose()
  fullName!: string;

  @Expose()
  username!: string;

  @Expose()
  accessToken!: string;
}
