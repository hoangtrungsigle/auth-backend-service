import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateUserTable1703084020775 implements MigrationInterface {
    name = 'CreateUserTable1703084020775'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`users\` (\`created_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`is_deleted\` tinyint NOT NULL DEFAULT 0, \`user_id\` varchar(36) NOT NULL, \`full_name\` varchar(100) NOT NULL, \`username\` varchar(100) NOT NULL, \`password\` varchar(500) NOT NULL, INDEX \`IDX_0adc0a8834ea0f252e96d154de\` (\`full_name\`), INDEX \`IDX_fe0bb3f6520ee0469504521e71\` (\`username\`), PRIMARY KEY (\`user_id\`)) ENGINE=InnoDB`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX \`IDX_fe0bb3f6520ee0469504521e71\` ON \`users\``);
        await queryRunner.query(`DROP INDEX \`IDX_0adc0a8834ea0f252e96d154de\` ON \`users\``);
        await queryRunner.query(`DROP TABLE \`users\``);
    }

}
